#include <exception>
#include <iomanip>
#include <iostream>
#include <type_traits>
#include <system_error>
#include <vector>

#include "detail.h"
#include "file-descriptor.h"
#include "logger.h"
#include "system-error.h"

using ReadFD = common::FileDescriptor;
using WriteFD = common::FileDescriptor;

template<typename Rfd, typename Wfd>
auto createPairTemplated()
    -> std::pair<Rfd, Wfd>
{
    int fds[2];
    int rv = ::pipe(fds);
    if (rv == -1)
        throw common::SystemError("pipe");
    return {Rfd(fds[0]), Wfd(fds[1])};
}

auto createPair()
    -> std::pair<ReadFD, WriteFD>
{
    return createPairTemplated<ReadFD, WriteFD>();
}

template<typename L, typename R>
bool equals(const L &lhs, const R &rhs) {
    return std::equal(lhs.begin(), lhs.end(),
                      rhs.begin(), rhs.end(),
                      [](auto l, auto r) { return char(l) == char(r); });
}

template<typename F>
bool expectError(int code, F f, const source::Location &where = source::Location::current()) {
    try {
        f();
        log::here(where) << "Expected error " << ::strerror(code);
        return false;
    } catch (const common::SystemError &e) {
        if (e.code().value() != code) {
            log::here(where) << "Expected error " << ::strerror(code) << "; got " << e.code().message();
            return false;
        }
        return true;;
    }
}

template<typename F>
bool suppressError(F f) {
    try {
        f();
        return true;
    } catch (const common::SystemError &) {
        return false;
    }
}

void testProperties() {
    if constexpr (!std::is_default_constructible_v<common::FileDescriptor>)
        log::here() << "FileDescriptor should be default constructible";
    if constexpr (std::is_copy_constructible_v<common::FileDescriptor>)
        log::here() << "FileDescriptor should not be copy constructible";
    if constexpr (std::is_copy_assignable_v<common::FileDescriptor>)
        log::here() << "FileDescriptor should not be copy assignable";
    if constexpr (!std::is_move_constructible_v<common::FileDescriptor>)
        log::here() << "FileDescriptor should be move constructible";
    if constexpr (!std::is_move_assignable_v<common::FileDescriptor>)
        log::here() << "FileDescriptor should be move assignable";
    if constexpr (std::is_polymorphic_v<common::FileDescriptor>)
        log::here() << "FileDescriptor should not have virtual methods";
    if constexpr (!detail::hasSwap<common::FileDescriptor>)
        log::here() << "FileDescriptor should have swap method";
}

template<typename FD>
void testSwap() {
    using namespace std::literals;
    if constexpr (detail::hasSwap<FD>) {
        std::array<char, 2> inBuffer;
        std::string_view outBuffer = "AABB";
        auto [ in, out ] = createPairTemplated<FD, FD>();

        auto rv = out.write(outBuffer);
        if (rv != outBuffer.size()) {
            log::here() << "`out' should be writeable";
            return;
        }
        in.swap(out);

        if (!expectError(EBADF, [&]{ rv = in.read(inBuffer);}))
            log::here() << "`in' after swap should not be readable";

        rv = out.read(inBuffer);
        if (rv != inBuffer.size())
            log::here() << "`out' after swap should be readable " << rv;

        if (auto expected = "AA"sv; !equals(inBuffer, expected))
            log::here() << "`inBuffer' does not contain expected values [" << expected << "]";
    }
    else {
        log::here() << "cannot run";
    }
}

template<typename FD>
void testOperators() {
    if constexpr (detail::isComparable<FD>) {
        auto [ in, out ] = createPairTemplated<FD, FD>();

        if (in == out)
            log::here() << "operator== is poorly implemented";
        if (in != in)
            log::here() << "operator!= is poorly implemented";
        if (!(in == in))
            log::here() << "operator== is poorly implemented";
        if (!(out != in))
            log::here() << "operator!= is poorly implemented";
    }
    else {
        if constexpr (!detail::hasEqual<FD>)
            log::here() << "FileDescriptor should define operator==";
        if constexpr (!detail::hasNonEqual<FD>)
            log::here() << "FileDescriptor should define operator!=";
    }
    if constexpr (detail::isOrderable<FD>) {
        auto [ in, out ] = createPairTemplated<FD, FD>();

        if ((in < out && out < in) || !(in < out || out < in))
            log::here() << "something is wrong with operator<";

        if ((in <= out && out <= in) || !(in <= out || out <= in))
            log::here() << "something is wrong with operator<=";

        if ((in > out && out > in) || !(in > out || out > in))
            log::here() << "something is wrong with operator>";

        if ((in >= out && out >= in) || !(in >= out || out >= in))
            log::here() << "something is wrong with operator>=";

        if ((in < out) != (out > in))
            log::here() << "something is wrong with operator< or operator>";
        if ((in <= out) != (out >= in))
            log::here() << "something is wrong with operator<= or operator>=";

        if ((in < out) != (in <= out))
            log::here() << "something is wrong with operator< or operator<=";
        if ((in > out) != (in >= out))
            log::here() << "something is wrong with operator> or operator>=";

    }
    else {
        if constexpr (!detail::hasLessThan<FD>)
            log::here() << "FileDescriptor should define operator<";
        if constexpr (!detail::hasLessThanOrEqThan<FD>)
            log::here() << "FileDescriptor should define operator<=";
        if constexpr (!detail::hasGreaterThan<FD>)
            log::here() << "FileDescriptor should define operator>";
        if constexpr (!detail::hasGreaterThanOrEq<FD>)
            log::here() << "FileDescriptor should define operator>=";
    }

}

void testStringReadWrite() {
    std::string inBuffer;
    std::string_view outBuffer = "hello here";
    inBuffer.reserve(outBuffer.size());

    auto [ in, out ] = createPair();
    out.write(outBuffer);
    in.read(inBuffer);
    if (!equals(inBuffer, outBuffer))
        log::here() << quoted(outBuffer) << " != " << quoted(inBuffer);
}

void testVectorReadWrite() {
    std::vector<std::byte> inBuffer;
    std::vector<signed char> outBuffer = { 'h', 'e', 'l', 'l', 'o' };
    inBuffer.reserve(outBuffer.size());

    auto [ in, out ] = createPair();
    out.write(outBuffer);
    in.read(inBuffer);

    if (!equals(inBuffer, outBuffer))
        log::here() << "mismatch";
}

template<typename FD>
void testDefaultConstructible() {
    if constexpr (std::is_default_constructible_v<FD>) {
        FD invalid;
        expectError(EBADF, [&]{ invalid.write("something"); });
    }
    else {
        log::here() << "cannot run";
    }
}

void testMoveConstructible() {
    std::string inBuffer;
    std::string_view outBuffer = "hello here";
    inBuffer.reserve(outBuffer.size());

    auto [ in, out ] = createPair();

    auto newIn = std::move(in);

    out.write(outBuffer);

    if (!expectError(EBADF, [&,&in = in]{ in.read(inBuffer); })) {
        log::here() << "`in' should not be readable";
    }
    else if (auto rv = newIn.read(inBuffer)) {
        if (!equals(inBuffer, outBuffer))
            log::here() << quoted(outBuffer) << " != " << quoted(inBuffer);
    } else {
        log::here() << "`newIn' should be readable";
    }
}

void testMoveAssignable() {
    using namespace std::literals;
    std::array<char, 2> inBuffer;
    auto outBufferA = "Avalue"sv;
    auto outBufferB = "Bvalue"sv;

    auto [ inA, outA ] = createPair();
    auto [ inB, outB ] = createPair();

    if (!suppressError([&,&outA = outA]{ outA.write(outBufferA); }))
        log::here() << "`outA' should be writeable";
    if (!suppressError([&,&outB = outB]{ outB.write(outBufferB); }))
        log::here() << "`outB' should be writeable";

    inB = std::move(inA);

    if (!expectError(EBADF, [&,&inA = inA]{ inA.read(inBuffer); }))
        log::here() << "`inA' should not be readable";
    if (!suppressError([&,&inB = inB]{ inB.read(inBuffer); }))
        log::here() << "`inB' should be readable";
}

int main() try {
    ::alarm(3);

    auto tests = {
        testProperties,
        testSwap<common::FileDescriptor>,
        testOperators<common::FileDescriptor>,
        testStringReadWrite,
        testVectorReadWrite,
        testDefaultConstructible<common::FileDescriptor>,
        testMoveConstructible,
        testMoveAssignable,
    };

    for (auto test : tests) {
        int before = detail::openedFileDescriptors();
        try {
            test();
        } catch (const std::exception &e) {
            log::here() << "Error: " << e.what();
        }

        int after = detail::openedFileDescriptors();
        if (before < after)
            log::here() << "Leaking "<< (after - before) <<" descriptors (" << before << " -> " << after << ")";
    }

    return 0;
} catch (const std::exception &e) {
    log::here() << "Unrecoverable error: " << e.what();
}
