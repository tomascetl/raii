#ifndef DETAIL_H
#define DETAIL_H

#include <errno.h>
#include <poll.h>
#include <sys/resource.h>
#include <system_error>
#include <vector>

namespace detail {

template<typename T>
std::add_lvalue_reference_t<T> decllval() noexcept;

template<typename T, typename U, typename = bool>
struct HasEqual : std::false_type {};

template<typename T, typename U>
struct HasEqual<T, U, decltype(std::declval<const T>() == std::declval<const U>())> : std::true_type {};

template<typename T, typename U, typename = bool>
struct HasNonEqual : std::false_type {};

template<typename T, typename U>
struct HasNonEqual<T, U, decltype(std::declval<const T>() != std::declval<const U>())> : std::true_type {};

template<typename T, typename U, typename = bool>
struct HasLessThan : std::false_type {};

template<typename T, typename U>
struct HasLessThan<T, U, decltype(std::declval<const T>() < std::declval<const U>())> : std::true_type {};

template<typename T, typename U, typename = bool>
struct HasLessThanOrEq : std::false_type {};

template<typename T, typename U>
struct HasLessThanOrEq<T, U, decltype(std::declval<const T>() <= std::declval<const U>())> : std::true_type {};

template<typename T, typename U, typename = bool>
struct HasGreaterThan : std::false_type {};

template<typename T, typename U>
struct HasGreaterThan<T, U, decltype(std::declval<const T>() > std::declval<const U>())> : std::true_type {};

template<typename T, typename U, typename = bool>
struct HasGreaterThanOrEq : std::false_type {};

template<typename T, typename U>
struct HasGreaterThanOrEq<T, U, decltype(std::declval<const T>() >= std::declval<const U>())> : std::true_type {};

template<typename T, typename = void>
struct HasSwap : std::false_type {};

template<typename T>
struct HasSwap<T, decltype(std::declval<T>().swap(decllval<T>()))> : std::true_type {};


template<typename T, typename U = T>
constexpr bool hasEqual = HasEqual<T, U>::value;

template<typename T, typename U = T>
constexpr bool hasNonEqual = HasNonEqual<T, U>::value;

template<typename T, typename U = T>
constexpr bool hasLessThan = HasLessThan<T, U>::value;

template<typename T, typename U = T>
constexpr bool hasLessThanOrEqThan = HasLessThanOrEq<T, U>::value;

template<typename T, typename U = T>
constexpr bool hasGreaterThan = HasGreaterThan<T, U>::value;

template<typename T, typename U = T>
constexpr bool hasGreaterThanOrEq = HasGreaterThanOrEq<T, U>::value;

template<typename T, typename U = T>
constexpr bool isComparable = hasEqual<T, U> && hasNonEqual<T, U>;

template<typename T, typename U = T>
constexpr bool isOrderable = hasLessThan<T, U> && hasLessThanOrEqThan<T, U>
                          && hasGreaterThan<T, U> && hasGreaterThanOrEq<T, U>;


template<typename T>
constexpr bool hasSwap = HasSwap<T>::value;

int openedFileDescriptors() {
    struct rlimit rlim;
    int rv = ::getrlimit(RLIMIT_NOFILE, &rlim);
    if (rv == -1)
        throw std::system_error(errno, std::generic_category(), "getrlimit");

    std::vector<struct pollfd> fds;
    for (int i = 0; i < rlim.rlim_cur; ++i)
        fds.push_back({i, 0, 0});
    
    rv = ::poll(fds.data(), fds.size(), 0);
    if (rv == -1)
        throw std::system_error(errno, std::generic_category(), "poll");

    int number = 0;
    for (struct pollfd &fd : fds) {
        if ((fd.revents & POLLNVAL) == 0)
            ++number;
    }
    return number;
}



} // namespace detail

#endif

