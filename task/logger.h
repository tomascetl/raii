#ifndef COMMON_LOGGER_H
#define COMMON_LOGGER_H

#include <cstring>
#include <cxxabi.h>
#include <memory>
#include <optional>
#include <string>
#include <string_view>
#include <sstream>
#include <ostream>
#include <typeinfo>

#include "location.h"

namespace log {

struct Global {
    /**
     * @brief Initialize default logger sink to std::clog.
     * 
     */
    constexpr Global()
        : _sink(defaultSink())
    {}

    /**
     * @brief Replace the current sink with any other.
     * @warning Not thread safe!
     * 
     * @tparam Sink The desired sink to be used for logging.
     * @tparam Args Type pack of @p args.
     * @param args Arguments needed to create the instance of the sink.
     * @return void
     */
    template<typename Sink, typename... Args>
    auto set(Args &&... args)
        -> std::enable_if_t<std::is_base_of_v<std::ostream, Sink>>
    {
        _storage = std::make_unique<Sink>(std::forward<Args>(args)...);
        _sink = _storage.get();
    }

    std::ostream &get() const {
        return *_sink;
    }

private:
    constexpr static std::ostream *defaultSink() {
        return &std::clog;
    }

    std::ostream *_sink;
    std::unique_ptr<std::ostream> _storage;
};

inline Global sink;

using source::Location;

template<typename Self, bool appendDots = true>
struct SelfNameResolver {
    static std::string_view name() noexcept {
        static DemangledName name(typeid(Self).name());
        return name.value();
    }

private:
    class DemangledName {
        char *_name;
        std::string_view _fallback;
    public:
        enum DemanglerStatus {
            OK = 0,
            ALLOCATION_FAIL = -1,
            INVALID_FORMAT = -2,
            INVALID_ARGUMENTS = -3
        };

        DemangledName(const char *mangled)
            : _name(nullptr)
        {
            using namespace std::literals;
            std::size_t n = 0;
            int status = 0;
            char *base = abi::__cxa_demangle(mangled, nullptr, &n, &status);
            switch (status) {
            case OK:
                if constexpr (appendDots) {
                    _name = static_cast<char *>(std::realloc(base, n + 2));
                    _name = _name ? std::strcat(_name, "::") : base;
                }
                else {
                    _name = base;
                }
                break;

            case ALLOCATION_FAIL:
                std::terminate();

            case INVALID_FORMAT:
                _fallback = "<could not demangle>"sv;
                break;

            case INVALID_ARGUMENTS:
                _fallback = "<demangled error>"sv;
                break;
            }
        }

        ~DemangledName() {
            std::free(_name);
        }

        std::string_view value() const noexcept {
            return _name ? _name : _fallback;
        }
    };
};

template<bool _>
struct SelfNameResolver<void, _> {
    static std::string_view name() noexcept {
        return {};
    }
};

template<typename _>
struct TrueType : std::true_type {};

struct Line {
    Line(std::ostream &out, const Location &here, std::string_view decoration = {})
        : _out(&out)
        , _here(here)
    {
        stream() << decoration << _here.function_name() << "(): ";
    }

    Line(const Line &) = delete;
    Line(Line &&other)
        : _out(std::exchange(other._out, nullptr))
        , _here(other._here)
    {}

    Line &operator=(const Line &) = delete;
    Line &operator=(Line &&) = default;

    ~Line() {
        using namespace std::literals;
        if (hasStream()) {
            stream() << " ["sv << _here.file_name() << ":" << _here.line() << "]" << std::endl;
        }
    }

    template<typename T>
    friend auto operator<<(Line &&line, const T &type)
        -> std::enable_if_t<TrueType<decltype(std::declval<std::ostream>() << type)>::value, Line>
    {
        line.stream() << type;
        return std::move(line);
    }

private:
    std::ostream &stream() {
        return *_out;
    }

    bool hasStream() const {
        return bool(_out);
    }

    std::ostream  *_out;
    Location _here;
};

template<typename Self>
struct Enable {
protected:
    static Line here(const Location &where = Location::current()) {
        return {sink.get(), where, SelfNameResolver<Self>::name()};
    }

public:
    struct WithName {
        using Static = Enable<Self>;
        WithName()
            : WithName(this)
        {}

        template<typename T>
        WithName(const T &decoration)
            : _name(addDecoration(SelfNameResolver<Self, false>::name(), decoration))
        {}

        WithName(const WithName &) = default;
        WithName(WithName &&) = default;
        WithName(const Self &self)
            : WithName(static_cast<const WithName &>(self))
        {}
        WithName(Self &&self)
            : WithName(std::move(static_cast<WithName &&>(self)))
        {}

    protected:
        Line here(const Location &where = Location::current()) const {
            return {sink.get(), where, _name};
        }

    private:
        template<typename T>
        std::string addDecoration(std::string_view name, const T &decoration) {
            std::ostringstream out;
            out << name << "(" << decoration << ")::";
            return std::move(out).str();
        }

        std::string _name;
    };
};

inline Line here(const Location &where = Location::current()) {
    return {sink.get(), where};
}


} // namespace log

#endif
